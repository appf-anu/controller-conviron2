# controller-conviron

* ![master:docker-build](https://gitlab.com/appf-anu/controller-conviron2/badges/master/pipeline.svg?job=build_master) *master:docker-build*

* ![master:go-lint](https://gitlab.com/appf-anu/controller-conviron2/badges/master/pipeline.svg?job=golint) *master:go-lint*

* ![master:go-staticcheck](https://gitlab.com/appf-anu/controller-conviron2/badges/master/pipeline.svg?job=staticcheck) *master:go-staticcheck*

* ![master:go_vet](https://gitlab.com/appf-anu/controller-conviron2/badges/master/pipeline.svg?job=go_vet) *master:go-vet*

* ![master:go_test](https://gitlab.com/appf-anu/controller-conviron2/badges/master/pipeline.svg?job=go_test) *master:go-test*

* ![master:go_build](https://gitlab.com/appf-anu/controller-conviron2/badges/master/pipeline.svg?job=go_build) *master:go-build*


network telnet controller for conviron growth chambers

Controls and gathers data from conviron chambers.

WARNING: this code uses an undocumented and unsupported api from conviron. It will probably break your chamber, do not complain to me if it destroys your hardware.


## Usage

### Options
```
--conditions: Path of the chamber conditions input file.
--loop: Loop over the conditions for the first day, ignoring the rest of the conditions file.
--interval: The interval for recording device metrics, in minutes. If not provided, the default interval is 10 minutes.
--no-metrics: Don’t collect device metrics.
--use-http: Use http to get device metrics instead of telnet.
--dummy: A “dummy run” - don’t send conditions to the chamber.

The following tags can be used to group the metrics:
--host-tag: name/ID of chamber
--group-tag: chamber group (e.g. spc, nonspc)
--did-tag: data ID - e.g. experiment/trial ID
```
