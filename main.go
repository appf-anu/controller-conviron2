package main

import (
	"encoding/xml"
	"flag"
	"fmt"
	"github.com/ziutek/telnet"
	ctools "gitlab.com/appf-anu/chamber-tools"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"os"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var (
	errLog *log.Logger
)

var (
	noMetrics, dummy, loopFirstDay bool
	useHTTP                        bool
	address                        string
	conditionsPath                 string
	tags                           ctools.TelegrafTags
	interval                       time.Duration
)

const (
	//matchFloatExp = `[-+]?\d*\.\d+|\d+`
	matchIntsExp = `\b(\d+)\b`
)

// matchFloat regex here is provided as reference
//var /* const */ matchFloat = regexp.MustCompile(matchFloatExp)
var /* const */ matchInts = regexp.MustCompile(matchIntsExp)

//const httpAuthString = "YWRtaW46ZmFkbWlu"

type pCOWeb struct {
	PCO pCO
}

type pCO struct {
	Analog  []analogVar  `xml:"ANALOG>VARIABLE"`
	Integer []integerVar `xml:"INTEGER>VARIABLE"`
}

type analogVar struct {
	//Index int `xml:"INDEX"`
	Value float64 `xml:"VALUE"`
}

type integerVar struct {
	//Index int `xml:"INDEX"`
	Value int `xml:"VALUE"`
}

var (
	// this is used because the convirons do not have an understanding of floating point numbers,
	// therefore 21.6c == 216 is used
	temperatureMultiplier = 10.0

	// these values are for controlling chambers
	temperatureDataIndex = 105
	humidityDataIndex    = 106
	light1DataIndex      = 107
	light2DataIndex      = 108
	//
	//// Conviron Control Sequences
	//// Give as a comma-seperated list of strings, each string consisting of
	////  "<Datatype> <Index> <Value>"
	//
	//// The init sequence is a sequence of strings passed to the set command which
	//// "setup" the conviron PCOweb controller to receive the temperature, humidity, and light settings.
	initCommand = "pcoset 0 I 100 26; pcoset 0 I 101 1; pcoset 0 I 102 1;"

	//// The teardown sequence happens at the end of each set of messages

	//// (not at the end of the connection)
	teardownCommand = "pcoset 0 I 121 1;"

	//// Command to clear the write flag, occurs after writing but before reloading.
	clearWriteFlagCommand = "pcoset 0 I 120 0;"

	//// Sequence to force reloading of the schedule, to make the written changes go live
	reloadSequence = "pcoset 0 I 100 7; pcoset 0 I 101 1; pcoset 0 I 102 1"
	//
	//// Command to clear the busy flag, occurs before exiting the connection
	clearBusyFlagCommand = "pcoset 0 I 123 0;"
	//// Command to set the busy flag
	setBusyFlagCommand             = "pcoset 0 I 123 1;"
	getChamberTimeCommand          = "pcoget 0 I 149 2;"
	secondaryGetChamberTimeCommand = "pcoget 0 I 45 2;"
)

//const (
// it is extremely unlikely (see. impossible) that we will be measuring a humidity of 214,748,365 %RH or a
// temperature of -340,282,346,638,528,859,811,704,183,484,516,925,440°C until we invent some new physics, so until
// then, I will use these values as the unset or null values for HumidityTarget and TemperatureTarget
//nullTargetInt   = math.MinInt32
//ctools.NullTargetFloat64 = -math.MaxFloat32
//)

// conviron indices start at 1

// AValues type represent the temperature values for the chamber (I dont know why these are on a different row to
// everything else, but they are. They also all require dividing by 10.0 because they are returned as integers.)
type AValues struct {
	Temperature         float64 `idx:"1" multiplier:"10.0"`
	TemperatureTarget   float64 `multiplier:"10.0"`
	TemperatureSetPoint float64 `idx:"2" multiplier:"10.0"`
	CoilTemperature     float64 `idx:"3" multiplier:"10.0"`
}

// IValues type represents the other values that aren't temperature, like relative humidity and par
type IValues struct {
	Success                             string
	HeatCoolModulatingProportionalValve int `idx:"1"`
	RelativeHumidity                    int `idx:"4"`
	RelativeHumidityTarget              int
	RelativeHumiditySetPoint            int `idx:"5"`
	RelativeHumidityAdd                 int `idx:"6"`
	Par                                 int `idx:"11"`
	Light1Target                        int
	Light1SetPoint                      int `idx:"12"`
	//Light1SetPoint                      int `idx:"23"`
	Light2Target   int
	Light2SetPoint int `idx:"13"`
	//Light2SetPoint						int `idx:"24"`

	HiPressure int `idx:"33"`
	LoPressure int `idx:"34"`
	//IPAddressOctet1						int `idx:"47"`
	//IPAddressOctet2						int `idx:"48"`
	//IPAddressOctet3						int `idx:"49"`
	//IPAddressOctet4						int `idx:"50"`
}

// DecodeValues decodes values in the array `values` and sets the values in the struct based on the `idx` tag,
// it also divides the values by the multiplier tag (which should be of the same type as the value).
func DecodeValues(values []int, i interface{}) error {

	v := reflect.ValueOf(i)

	if v.Kind() != reflect.Ptr || v.IsNil() {
		return fmt.Errorf("decode requires non-nil pointer")
	}
	// get the value that the pointer v points to.
	v = v.Elem()
	// get type of v
	t := v.Type()

	for i := 0; i < v.NumField(); i++ {
		ft := t.Field(i)
		// skip unexported fields. from godoc:
		// PkgPath is the package path that qualifies a lower case (unexported)
		// field name. It is empty for upper case (exported) field names.
		if ft.PkgPath != "" {
			continue
		}
		fv := v.Field(i)
		if idxString, ok := ft.Tag.Lookup("idx"); ok {
			if idx, err := strconv.ParseInt(idxString, 10, 64); err == nil {
				// the conviron idx starts at 1
				idx = idx - 1
				switch fv.Kind() {
				case reflect.Int:
					value := int64(values[idx])
					if multiplierString, ok := ft.Tag.Lookup("multiplier"); ok {
						if mult, err := strconv.ParseInt(multiplierString, 10, 64); err == nil {
							value /= mult
						}
					}

					fv.SetInt(value)
				case reflect.Float64, reflect.Float32:
					floatVal := float64(values[idx])
					if multiplierString, ok := ft.Tag.Lookup("multiplier"); ok {
						if mult, err := strconv.ParseFloat(multiplierString, 64); err == nil {
							floatVal /= mult
						}
					}

					fv.SetFloat(floatVal)
				case reflect.Bool:
					fv.SetBool(values[idx] != 0)
				}

			}

		}
	}
	return nil
}

var usage = func() {
	use := `
usage of %s:
flags:
	-no-metrics: don't send metrics to telegraf
	-dummy: don't control the chamber, only collect metrics (this is implied by not specifying a conditions file
	-conditions: conditions to use to run the chamber
	-interval: what interval to run conditions/record metrics at, set to 0s to read 1 metric and exit. (default=10m)

examples:
	collect data on 192.168.1.3  and output the errors to GC03-error.log and record the output to GC03.log
	%s -dummy 192.168.1.3 2>> GC03-error.log 1>> GC03.log

	run conditions on 192.168.1.3  and output the errors to GC03-error.log and record the output to GC03.log
	%s -conditions GC03-conditions.csv -dummy 192.168.1.3 2>> GC03-error.log 1>> GC03.log

quirks:
	the first 3 or 4 columns are used for running the chamber:
		date,time,temperature,humidity OR datetime,temperature,humidity
		the second case only occurs if the first 8 characters of the file (0th header) is "datetime"

	for the moment, the first line of the csv is technically (this is for your headers)
	if both -dummy and -no-metrics are specified, this program will exit.

`
	fmt.Printf(use, os.Args[0], os.Args[0], os.Args[0])
}

func chompAllValues(conn *telnet.Conn, command string) (values []int, err error) {

	// write command
	conn.Write([]byte(command + "\n"))
	// read 1 newline
	err = conn.SkipUntil("\n")
	if err != nil {
		return
	}

	// read another coz previous would be ours
	rawData, err := conn.ReadString('#')

	if err != nil {
		return
	}
	// trim...
	trimmedData := strings.TrimSpace(rawData)
	// find the ints
	tmpStrings := matchInts.FindAllString(trimmedData, -1)
	for _, v := range tmpStrings {
		i, err := strconv.ParseInt(v, 10, 64)
		if err != nil {
			return values, err
		}
		values = append(values, int(i))
	}
	return
}

func getValuesHTTP(a *AValues, i *IValues) (err error) {

	redirectPolicyFunc := func(req *http.Request, via []*http.Request) error {
		req.SetBasicAuth("admin", "fadmin")
		return nil
	}
	client := &http.Client{
		CheckRedirect: redirectPolicyFunc,
	}
	req, _ := http.NewRequest("GET", "http://"+address+"/config/xml.cgi?I|1|35|A|1|3", nil)
	req.SetBasicAuth("admin", "fadmin")
	resp, err := client.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	container := pCOWeb{}
	if resp.StatusCode != http.StatusOK {
		errLog.Println("status code ", resp.StatusCode)
	}
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	err = xml.Unmarshal(bodyBytes, &container)
	if err != nil {
		return
	}
	var aValues []int
	for _, x := range container.PCO.Analog {
		aValues = append(aValues, int(x.Value*10.0)) // analog values are all multiply by 10
	}
	var iValues []int
	for _, x := range container.PCO.Integer {
		iValues = append(iValues, int(x.Value))
	}
	err = DecodeValues(aValues, a)
	if err != nil {
		return
	}
	err = DecodeValues(iValues, i)
	if err != nil {
		return
	}
	return
}

func getValues(a *AValues, i *IValues) (err error) {

	errLog.Println("Attempting telnet connection ...")
	conn, err := telnet.DialTimeout("tcp", address, time.Second*30)
	if err != nil {
		return
	}
	conn.SetUnixWriteMode(true)
	defer conn.Close()
	err = login(conn)
	if err != nil {
		return
	}

	aValues, err := chompAllValues(conn, "pcoget 0 A 1 3")
	if err != nil {
		return
	}
	iValues, err := chompAllValues(conn, "pcoget 0 I 1 35")
	if err != nil {
		return
	}
	err = DecodeValues(aValues, a)
	if err != nil {
		return
	}
	err = DecodeValues(iValues, i)
	if err != nil {
		return
	}
	return
}

func writeValues(a *AValues, i *IValues) (err error) {
	start := time.Now()
	defer func() {
		errLog.Println("time for last writeValues: ", time.Since(start))
	}()

	conn, err := telnet.DialTimeout("tcp", address, time.Second*30)
	if err != nil {
		return
	}
	conn.SetUnixWriteMode(true)
	defer conn.Close()
	err = login(conn)
	if err != nil {
		return
	}

	// if this results in no error, then we got the time in the chamber
	if t, err := chompAllValues(conn, getChamberTimeCommand); err == nil {
		if len(t) == 2 && t[0] == 0 && t[1] == 0 {
			// if they are both 0 then bingo, its midnight and the controller is busy reloading the program.
			errLog.Println("midnight in chamber, skipping...")
			return nil
		}
		if t2, err := chompAllValues(conn, secondaryGetChamberTimeCommand); err == nil {
			if len(t2) == 2 && t2[0] == 0 && t2[1] == 0 {
				return nil
			}
		}

	} else {
		return nil
	}

	// set busy flag
	if _, err = chompAllValues(conn, setBusyFlagCommand); err != nil {
		return
	}
	time.Sleep(time.Millisecond * 100)
	if _, err = chompAllValues(conn, initCommand); err != nil {
		return
	}
	// temperature
	if a.TemperatureTarget != ctools.NullTargetFloat64 {
		temperatureIntValue := int(a.TemperatureTarget * temperatureMultiplier)
		temperatureCommand := fmt.Sprintf("pcoset 0 I %d %d; ",
			temperatureDataIndex,
			temperatureIntValue)
		if _, err = chompAllValues(conn, temperatureCommand); err != nil {
			return
		}
	}

	// humidity
	if i.RelativeHumidityTarget != ctools.NullTargetInt {
		humidityCommand := fmt.Sprintf("pcoset 0 I %d %d; ",
			humidityDataIndex,
			i.RelativeHumidityTarget)
		if _, err = chompAllValues(conn, humidityCommand); err != nil {
			return
		}
	}
	// light1 & light2
	if i.Light1Target != ctools.NullTargetInt {
		light1Command := fmt.Sprintf("pcoset 0 I %d %d; ", light1DataIndex, i.Light1Target)
		if _, err = chompAllValues(conn, light1Command); err != nil {
			return
		}
	}
	if i.Light2Target != ctools.NullTargetInt {
		light2Command := fmt.Sprintf("pcoset 0 I %d %d; ", light2DataIndex, i.Light2Target)
		if _, err = chompAllValues(conn, light2Command); err != nil {
			return
		}
	}
	// teardown
	if _, err = chompAllValues(conn, teardownCommand); err != nil {
		return
	}
	// wait a bit
	time.Sleep(time.Second * 2)

	// do the rest of the stuff to release the chamber
	if _, err = chompAllValues(conn, clearWriteFlagCommand); err != nil {
		return
	}
	if _, err = chompAllValues(conn, reloadSequence); err != nil {
		return
	}
	if _, err = chompAllValues(conn, teardownCommand); err != nil {
		return
	}
	// make sure its released and cleared
	time.Sleep(time.Second * 2)
	if _, err = chompAllValues(conn, clearWriteFlagCommand); err != nil {
		return
	}
	if _, err = chompAllValues(conn, clearBusyFlagCommand); err != nil {
		return
	}

	return
}

func login(conn *telnet.Conn) (err error) {
	time.Sleep(time.Second * 1)
	err = conn.SkipUntil("login: ")
	if err != nil {
		return
	}

	conn.Write([]byte("root\n"))
	time.Sleep(time.Second * 1)
	err = conn.SkipUntil("Password: ")
	if err != nil {
		return
	}

	conn.Write([]byte("froot\n"))
	time.Sleep(time.Second * 1)
	err = conn.SkipUntil("# ")
	if err != nil {
		return
	}
	// END login
	return
}

// runStuff, should send values and write metrics.
// returns true if program should continue, false if program should retry
func runStuff(point *ctools.TimePoint) bool {
	// round temperature to 1 decimal place
	// handle nulls
	a := AValues{TemperatureTarget: ctools.NullTargetFloat64}
	if point.Temperature != ctools.NullTargetFloat64 {
		a = AValues{TemperatureTarget: math.Round(point.Temperature*10) / 10}
	}
	// round humidity to nearest integer
	// handle nulls, IValues use nullTargetInt
	i := IValues{
		RelativeHumidityTarget: ctools.NullTargetInt,
		Light1Target:           point.Light1,
		Light2Target:           point.Light2,
	}
	if point.RelativeHumidity != ctools.NullTargetFloat64 {
		i.RelativeHumidityTarget = int(math.Round(point.RelativeHumidity))
	}

	err := getValues(&a, &i)
	if err != nil {
		errLog.Println(err)
		time.Sleep(time.Second * 10)
		return false
	}
	errLog.Printf("%s (tgt|setp|act) t:(%.1f|%.1f|%.1f) rh:(%02d|%02d|%02d)",
		point.Datetime,
		a.TemperatureTarget,
		a.TemperatureSetPoint,
		a.Temperature,
		i.RelativeHumidityTarget,
		i.RelativeHumiditySetPoint,
		i.RelativeHumidity)

	if point.Light1 != ctools.NullTargetInt || point.Light2 != ctools.NullTargetInt {
		errLog.Printf("%s PAR: %d (tgt|setp) l1:(%01d|%01d) l2:(%01d|%01d)",
			point.Datetime,
			i.Par,
			i.Light1Target,
			i.Light1SetPoint,
			i.Light2Target,
			i.Light2SetPoint)
	}
	i.Success = "SUCCESS"
	if err = writeValues(&a, &i); err != nil {
		errLog.Println(err)
		i.Success = err.Error()
	}

	for x := 0; x < 5; x++ {
		if err := ctools.WriteMetricUDP("conviron2", tags, a); err != nil {
			errLog.Println(err)
			time.Sleep(200 * time.Millisecond)
			continue
		}
		break
	}
	for x := 0; x < 5; x++ {
		if err := ctools.WriteMetricUDP("conviron2", tags, i); err != nil {
			errLog.Println(err)
			time.Sleep(200 * time.Millisecond)
			continue
		}
		break
	}
	ev := ctools.GetEnvironmentalStats(a.Temperature, float64(i.RelativeHumidity))

	for x := 0; x < 5; x++ {
		if err := ctools.WriteMetricUDP("conviron2", tags, ev); err != nil {
			errLog.Println(err)
			time.Sleep(200 * time.Millisecond)
			continue
		}
		break
	}
	return true
}

func init() {
	var err error
	hostname := os.Getenv("NAME")

	errLog = log.New(os.Stderr, "[conviron] ", log.Ldate|log.Ltime|log.Lshortfile)

	// get the local zone and offset
	flag.Usage = usage
	flag.BoolVar(&noMetrics, "no-metrics", false, "dont collect metrics")
	if tempV := strings.ToLower(os.Getenv("NO_METRICS")); tempV != "" {
		if tempV == "true" || tempV == "1" {
			noMetrics = true
		} else {
			noMetrics = false
		}
	}

	errLog.Println()

	flag.BoolVar(&dummy, "dummy", false, "dont send conditions to chamber")
	if tempV := strings.ToLower(os.Getenv("DUMMY")); tempV != "" {
		if tempV == "true" || tempV == "1" {
			dummy = true
		} else {
			dummy = false
		}
	}

	flag.BoolVar(&useHTTP, "use-http", false, "use http to get metrics instead of telnet")
	if tempV := strings.ToLower(os.Getenv("USE_HTTP")); tempV != "" {
		if tempV == "true" || tempV == "1" {
			useHTTP = true
		} else {
			useHTTP = false
		}
	}

	flag.BoolVar(&loopFirstDay, "loop", false, "loop over the first day")
	if tempV := strings.ToLower(os.Getenv("LOOP")); tempV != "" {
		if tempV == "true" || tempV == "1" {
			loopFirstDay = true
		} else {
			loopFirstDay = false
		}
	}

	flag.StringVar(&tags.Host, "host-tag", hostname, "host tag to add to the measurements")
	if tempV := os.Getenv("HOST_TAG"); tempV != "" {
		tags.Host = tempV
	}

	flag.StringVar(&tags.Group, "group-tag", "nonspc", "host tag to add to the measurements")
	if tempV := os.Getenv("GROUP_TAG"); tempV != "" {
		tags.Group = tempV
	}

	flag.StringVar(&tags.DataID, "did-tag", "", "Data ID tag")
	if tempV := os.Getenv("DID_TAG"); tempV != "" {
		tags.DataID = tempV
	}

	flag.StringVar(&conditionsPath, "conditions", "", "conditions file to")
	if tempV := os.Getenv("CONDITIONS_FILE"); tempV != "" {
		conditionsPath = tempV
	}
	flag.DurationVar(&interval, "interval", time.Minute*10, "interval to run conditions/record metrics at")
	if tempV := os.Getenv("INTERVAL"); tempV != "" {
		interval, err = time.ParseDuration(tempV)
		if err != nil {
			errLog.Println("Couldnt parse interval from environment")
			errLog.Println(err)
		}
	}
	flag.Parse()

	if address = os.Getenv("ADDRESS"); address == "" {
		address = flag.Arg(0)
	}

	if noMetrics && dummy {
		errLog.Println("dummy and no-metrics specified, nothing to do.")
		os.Exit(1)
	}
	if conditionsPath != "" && !dummy {
		if tags.Group == "nonspc" {
			tags.Group = "spc"
		}
	}
	errLog.Printf("noMetrics: \t%t\n", noMetrics)
	errLog.Printf("dummy: \t%t\n", dummy)
	errLog.Printf("loopFirstDay: \t%t\n", loopFirstDay)
	errLog.Printf("timezone: \t%s\n", ctools.ZoneName)
	errLog.Printf("tags: \t%+v\n", tags)
	errLog.Printf("address: \t%s\n", address)
	errLog.Printf("file: \t%s\n", conditionsPath)
	errLog.Printf("interval: \t%s\n", interval)

}

func main() {
	if interval == time.Second*0 {
		a := AValues{TemperatureTarget: ctools.NullTargetFloat64}
		i := IValues{RelativeHumidityTarget: ctools.NullTargetInt, Light1Target: ctools.NullTargetInt, Light2Target: ctools.NullTargetInt}
		var err error
		if useHTTP {
			err = getValuesHTTP(&a, &i)
		} else {
			err = getValues(&a, &i)
		}
		if err != nil {
			errLog.Println(err)
			os.Exit(1)
		}

		fmt.Println(ctools.DecodeStructToLineProtocol("conviron2", tags, a))
		fmt.Println(ctools.DecodeStructToLineProtocol("conviron2", tags, i))
		ev := ctools.GetEnvironmentalStats(a.Temperature, float64(i.RelativeHumidity))
		fmt.Println(ctools.DecodeStructToLineProtocol("conviron2", tags, ev))

		os.Exit(0)
	}

	if !noMetrics && (conditionsPath == "" || dummy) {
		a := AValues{TemperatureTarget: ctools.NullTargetFloat64}
		i := IValues{RelativeHumidityTarget: ctools.NullTargetInt}

		var err error
		if useHTTP {
			err = getValuesHTTP(&a, &i)
		} else {
			err = getValues(&a, &i)
		}

		if err != nil {
			errLog.Println(err)
		} else {
			// print the line

			fmt.Println(ctools.DecodeStructToLineProtocol("conviron2", tags, a))
			for x := 0; x < 5; x++ {
				if err := ctools.WriteMetricUDP("conviron2", tags, a); err != nil {
					errLog.Println(err)
					time.Sleep(200 * time.Millisecond)
					continue
				}
				break
			}
			fmt.Println(ctools.DecodeStructToLineProtocol("conviron2", tags, i))
			for x := 0; x < 5; x++ {
				if err := ctools.WriteMetricUDP("conviron2", tags, i); err != nil {
					errLog.Println(err)
					time.Sleep(200 * time.Millisecond)
					continue
				}
				break
			}
			ev := ctools.GetEnvironmentalStats(a.Temperature, float64(i.RelativeHumidity))
			fmt.Println(ctools.DecodeStructToLineProtocol("conviron2", tags, ev))
			for x := 0; x < 5; x++ {
				if err := ctools.WriteMetricUDP("conviron2", tags, ev); err != nil {
					errLog.Println(err)
					time.Sleep(200 * time.Millisecond)
					continue
				}
				break
			}
		}
		ticker := time.NewTicker(interval)
		go func() {
			for range ticker.C {
				a := AValues{TemperatureTarget: ctools.NullTargetFloat64}
				i := IValues{
					RelativeHumidityTarget: ctools.NullTargetInt,
					Light1Target:           ctools.NullTargetInt,
					Light2Target:           ctools.NullTargetInt,
				}

				var err error
				if useHTTP {
					err = getValuesHTTP(&a, &i)
				} else {
					err = getValues(&a, &i)
				}
				if err != nil {
					errLog.Println(err)
					continue
				}


				fmt.Println(ctools.DecodeStructToLineProtocol("conviron2", tags, a))
				for x := 0; x < 5; x++ {
					if err := ctools.WriteMetricUDP("conviron2", tags, a); err != nil {
						errLog.Println(err)
						time.Sleep(200 * time.Millisecond)
						continue
					}
					break
				}
				fmt.Println(ctools.DecodeStructToLineProtocol("conviron2", tags, i))
				for x := 0; x < 5; x++ {
					if err := ctools.WriteMetricUDP("conviron2", tags, i); err != nil {
						errLog.Println(err)
						time.Sleep(200 * time.Millisecond)
						continue
					}
					break
				}
				ev := ctools.GetEnvironmentalStats(a.Temperature, float64(i.RelativeHumidity))
				fmt.Println(ctools.DecodeStructToLineProtocol("conviron2", tags, ev))
				for x := 0; x < 5; x++ {
					if err := ctools.WriteMetricUDP("conviron2", tags, ev); err != nil {
						errLog.Println(err)
						time.Sleep(200 * time.Millisecond)
						continue
					}
					break
				}
			}
		}()
		select {}
	}

	if conditionsPath != "" && !dummy {
		ctools.RunConditions(errLog, runStuff, conditionsPath, loopFirstDay)
	}

}
